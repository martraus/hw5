import java.util.*;

public class Tnode implements java.util.Iterator<Tnode> {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode (String s, Tnode c, Tnode si) {
      setName (s);
      setFirstChild (c);
      setNextSibling (si);
   }

   Tnode (String s) {
      this(s, null, null);
   }

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setNextSibling (Tnode s) {
      nextSibling = s;
   }

   public Tnode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild (Tnode c) {
      firstChild = c;
   }

   public Tnode getFirstChild() {
      return firstChild;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      if (firstChild != null) {
         b.append("(");
         b.append(firstChild);
      }
      if (nextSibling != null) {
         b.append(",");
         b.append(nextSibling);
         b.append(")");
      }
      return b.toString();
   }

   public boolean hasNext() {
      return (getNextSibling() != null);
   }

   public Tnode next() {
      return getNextSibling();
   }

   // idea from: http://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
   public static Tnode buildFromRPN (String pol) {
      Tnode root;

      if (pol == null)
         throw new RuntimeException("Avaldis " + pol + " on NULL.");
      if (pol.trim().length() < 1)
         throw new RuntimeException("Avaldis " + pol + " on tühi.");
      Stack<Tnode> stack = new Stack<Tnode>();

      StringTokenizer st = new StringTokenizer(pol);

         while (st.hasMoreTokens()) {
            String token = st.nextToken();
            Tnode node = new Tnode(token);

            if (token.compareTo("/") == 0 || token.compareTo("*") == 0 || token.compareTo("-") == 0 || token.compareTo("+") == 0) {
               if (stack.empty()) {
                  throw new RuntimeException("Avaldises " + pol + " pole piisavalt arve.");
               }
               node.setNextSibling(stack.pop());
               if (stack.empty()) {
                  throw new RuntimeException("Avaldises " + pol + " pole piisavalt arve.");
               }
               node.setFirstChild(stack.pop());
            } else
               try {
                  Integer.valueOf(token);
               } catch (NumberFormatException e) {
                  throw new RuntimeException("Avaldises " + pol + " on tundmatu sümbol " + token);
               }
            stack.push(node);
         }

      if (stack.size() != 1) {
         throw new RuntimeException("Avaldises " + pol + " on liiga palju arve.");
      }
      root = stack.pop();
      return root;
   }

   public static void main (String[] param) {
      //String rpn = "1 2 +";
      //String rpn = "7 4 + 3 -";
      //String rpn = "5 9 2 * +";
      String rpn = "1 2 * 3 4 * +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }
}

